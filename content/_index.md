---
title: "About"
---

🦄 Hi, I'm Kahlia -- a former pro athlete, turned AI engineer and Fulbright Scholar

🚀 I'm exploring the nexus of possibility between Sport x Climate Tech

🧩 I'm a tinkerer and a hacker, captivated by puzzles and solving complex problems

⚡ I'm passionate about high performance sport, MLOps, and AI driven social impact

🔥 I'm fuelled by an insatiable curiosity, unwavering grit, and the pursuit of excellence
